import React from "react";
import {} from "grommet";
import { Notification } from "grommet-icons";
import axios from "axios";
import { connect } from "react-redux";
import * as userActions from "../../store/users/actions";

import {
  grommet,
  Grommet,
  Box,
  Button,
  Collapsible,
  Heading,
  Text,
  List
} from "grommet";

const HorizontalCollapsible = () => {
  const [openNotification, setOpenNotification] = React.useState();
  const [employees, setEmployees] = React.useState([]);
  if (employees.length < 1) {
    axios.get("http://dummy.restapiexample.com/api/v1/employees").then(res => {
      setEmployees(res.data);
    });
  }
  if(Object.keys(employees).length===0){
    return "Please wait..";
  }

  return (
    <Grommet full theme={grommet}>
      <Box fill>
        <Box
          as="header"
          direction="row"
          align="center"
          pad={{ vertical: "small", horizontal: "medium" }}
          justify="between"
          background="neutral-3"
          elevation="large"
          style={{ zIndex: "1000" }}
        >
          <Heading level={3} margin="none" color="white">
            <strong>My App</strong>
          </Heading>
          <Button
            onClick={() => setOpenNotification(!openNotification)}
            icon={<Notification color="white" />}
          />
        </Box>
        <Box flex direction="row">
          <Box flex align="center" justify="center">
            Dashboard content goes here, click on the notification icon
          </Box>
          <Collapsible direction="horizontal" open={openNotification}>
            <Box
              flex
              width="medium"
              background="light-2"
              pad="small"
              elevation="small"
            >
              <Text size="xlarge">Sidebar</Text>
            </Box>
          </Collapsible>
        </Box>
        <List
          data={employees.data.slice(0, 10)}
          primaryKey={item => (
            <Text size="large" weight="bold">
              {item.employee_name}
            </Text>
          )}
          secondaryKey={item => (
            <Text size="small" color="dark-4">
              {item.employee_age}
            </Text>
          )}
        />
      </Box>
    </Grommet>
  );
};

export class Test extends React.Component {
  constructor(props) {
    super(props);
  }
  state = {
    TestActive: false
  };

  async componentDidMount() {
    this.load();
  }

  load = async () => {
    await this.props.dispatch(userActions.fetchUsers());
  };

  render() {
    return (
      <React.Fragment>
        <HorizontalCollapsible />
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    usersData: state.users.usersArray,
    meta: state.users.meta,
    isLoading: state.users.isLoading,
    error: state.users.error
  };
}

export default connect(mapStateToProps)(Test);
