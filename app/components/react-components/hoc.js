import React from "react";
import { createStore, applyMiddleware, combineReducers } from "redux";
import { Provider } from "react-redux";
import * as reducers from "../../store/reducers";
import thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";

const store = createStore(
  combineReducers(reducers),
  // applyMiddleware(thunk),
  composeWithDevTools(
    applyMiddleware(thunk)
    // other store enhancers if any
  )
);

const HocWrapper = WrappedComponent => {
  class HOC extends React.Component {
    render() {
      return (
        <React.Fragment>
          <Provider store={store}>
            <WrappedComponent {...this.props} />;
          </Provider>
        </React.Fragment>
      );
    }
  }
  return HOC;
};

export default HocWrapper;
