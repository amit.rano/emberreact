import ReactComponent from "../react-component";
import HocWrapper from "./react-components/hoc";
import Test from "./react-components/test";

const TestHoc = HocWrapper(Test);

export default ReactComponent.extend({
  didInsertElement() {
    this._super(...arguments);
    this.reactRender(<TestHoc />);
  }
});
