import * as types from "./actionTypes";

const initialState = {
  usersArray: [],
  userDetail: {},
  userData: {},
  isLoading: false,
  error: null,
  isUserUpdated: false,
  isUserCreated: false,
  isValidationError: false,
  meta: {}
};

export default function reduce(state = initialState, action = {}) {
  switch (action.type) {
    case types.RESET_ALL:
      return initialState;
    //Fetch Users

    case types.FETCH_USERS_BEGIN:
      return {
        ...state,
        isLoading: true,
        error: null
      };

    case types.FETCH_USERS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        userData: {},
        usersArray: action.payload.users,
        meta: action.payload.users,
        isUserUpdated: false,
        error: {},
        isUserCreated: false,
        isValidationError: false
      };

    case types.FETCH_USERS_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.payload.error,
        usersArray: [],
        meta: {}
      };

    default:
      return state;
  }
}
