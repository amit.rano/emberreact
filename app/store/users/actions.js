import * as types from "./actionTypes";

export function fetchUsers(params) {
  return async (dispatch, getState) => {
    dispatch(types.fetchUsersBegin());
    dispatch(types.fetchUsersSuccess([]));
  };
}

export function resetAll() {
  return async (dispatch, getState) => {
    dispatch(types.resetAll());
  };
}
